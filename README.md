# unicorn_screams

It is what it says on the tin. Make [scream](https://github.com/duncanthrax/scream) work

## THE CONTEXT

I have a windows machine (cursed), it is inside a NAT with my main personal
machine outside of it due to reasons (double cursed).
I want to have transparent control from my
linux personal machine into that windows machine (triple cursed).

I have configured [Tailscale](https://tailscale.com) between them so they have
a nice NAT-punched connection to talk through.

The transparent control part of it is successfully handled by
[barrier](https://github.com/debauchee/barrier/).

However, *audio streaming is not a feature of Barrier*.

I have attempted to use ffmpeg to capture audio via DirectShow from a VBCable
(because somehow my audio driver doesn't give me the Stereo Mix device), sending
it as RTP over tailscale, and it *works*, *kind of*.

The latency is **TERRIBLE** after a while, I don't fully know why. A certain
sentient machine suggested scream to me. It is an interesting, simple tool.

Problem is: *Tailscale does not support UDP Multicast*. How the fuck do I
connect the machines then????

This is where this program steps in.

It will take the inputs from scream's UDP address (you have to configure it
as unicast instead of multicast), and it'll simply relay them to another UDP
address (in my case, my Tailscale address)

Why not set the Tailscale address on scream's unicast settings?

It didn't work.

Fuck if I know why!!!

## Pre-setup

- Install the scream driver
- Configure unicast: https://github.com/duncanthrax/scream#using-unicast-instead-of-multicast-optional
  - In my setup, address is `127.0.0.1` and port is `6970`

## Setup

### Build

This can be done on a linux machine.

- Install [zig master branch](https://ziglang.org/download/).
- `zig build -Dtarget=x86_64-windows-gnu`
- If you're me: `sudo tailscale file cp ./zig-out/bin/unicorn_screams.exe your-machine:`

### Run

- Syntax: `.\unicorn_screams.exe from_address from_port to_address to_port`
    - In my setup: `.\unicorn_screams.exe 127.0.0.1 6970 100.123.69.69 6970`
- Load your scream receiver of choice
    - In my setup, with the default unix receiver, `./scream -u -i 100.123.69.69 -p 6970`
- [Play this test file on Windows](https://www.youtube.com/watch?v=FtutLA63Cp8)
